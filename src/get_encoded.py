import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import savgol_filter

from six.moves import xrange

import umap

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
import torch.optim as optim

import torchvision.datasets as datasets
import torchvision.transforms as transforms
from torchvision.utils import make_grid

from sklearn.model_selection import train_test_split

from dataset import CornDataset

import time

from model import Model

def show(img,path):
        npimg = img.numpy()
        fig = plt.imshow(np.transpose(npimg, (1,2,0)), interpolation='nearest')
        fig.axes.get_xaxis().set_visible(False)
        fig.axes.get_yaxis().set_visible(False)
        plt.savefig(path)
        plt.clf()

if __name__=="__main__":
    print("Loading device...")
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(device, " device loaded")

    print("Creating dataset...")
    data = CornDataset("./data",transforms.Compose([
                                      transforms.ToTensor(),
                                      transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)) # Imagenet values
                                  ]))
    print("Dataset created!")

    # training_data, validation_data = train_test_split(data, test_size=0.33, random_state=42)
    # print(training_data)
    data_variance = 0.225**2

    batch_size = 32
    num_training_updates = 10000

    num_hiddens = 128
    num_residual_hiddens = 32
    num_residual_layers = 2

    embedding_dim = 64
    num_embeddings = 512

    commitment_cost = 0.25

    decay = 0.99

    learning_rate = 1e-3

    print("Making loader")
    loader = DataLoader(data, 
                             batch_size=1, 
                             shuffle=True,
                             pin_memory=True)

    print("Making model")
    model = Model(num_hiddens, num_residual_layers, num_residual_hiddens,
              num_embeddings, embedding_dim, 
              commitment_cost, decay).to(device)
    
    print("Loading model")
    model.load_state_dict(torch.load("outputs/model.pt"))
    model.eval()
    
    enc_list = []
    for d in loader:
        label = d["label"]
        image = d["image"]
        image = image.to(device)
        encoded = model._encoder(image)
        output = model._pre_vq_conv(encoded)
        vq = model._vq_vae(output)[1]
        print(vq)
        break
    # valid_originals = next(iter(validation_loader))["image"]
    # valid_originals = valid_originals.to(device)

    # vq_output_eval = model._pre_vq_conv(model._encoder(valid_originals))
    # _, valid_quantize, _, _ = model._vq_vae(vq_output_eval)
    # valid_reconstructions = model._decoder(valid_quantize)

    # invTrans = transforms.Compose([ transforms.Normalize(mean = [ 0., 0., 0. ],
    #                                                  std = [ 1/0.229, 1/0.224, 1/0.225 ]),
    #                             transforms.Normalize(mean = [ -0.485, -0.456, -0.406 ],
    #                                                  std = [ 1., 1., 1. ]),
    #                            ])

    # rec_tensor = invTrans(valid_reconstructions)
    # orig_tensor = invTrans(valid_originals)

    # show(make_grid(rec_tensor.cpu().data), "outputs/visualization_rec.jpeg")
    # show(make_grid(orig_tensor.cpu().data), "outputs/visualization_org.jpeg")

    # proj = umap.UMAP(n_neighbors=3,
    #              min_dist=0.1,
    #              metric='cosine').fit_transform(model._vq_vae._embedding.weight.data.cpu())
    # plt.scatter(proj[:,0], proj[:,1], alpha=0.3)
    # plt.savefig("outputs/embeddings.jpeg")