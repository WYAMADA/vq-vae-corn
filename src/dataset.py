import glob
import os
import cv2
import torch
import numpy as np
from tqdm import tqdm
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
from skimage import io, transform

def get_files_path(path):
    files_path = glob.glob(os.path.join(path,"**/*.jpeg"),recursive=True)
    if len(files_path) == 0:
        print("Invalid file path: ", path)
    return files_path

class CornDataset(Dataset):
    def __init__(self, root_dir, transform=None):
        self.files = get_files_path(root_dir)
        self.transform = transform
    
    def __len__(self):
        return(len(self.files))
    
    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.to_list()
        
        path = self.files[idx]
        image = io.imread(path)
        # image = cv2.cvtColor(cv2.imread(path),cv2.COLOR_BGR2RGB)
        path_list = path.split("/")
        plot = path_list[-2].upper()
        if path_list[-3]=='Plot Study 1 - 09152021':
            ps = "PS1"
        elif path_list[-3]=='Plot Study 2 - 09202021':
            ps = "PS2"
        else:
            print("Error loading the image label")
        label = ps + "-" + plot

        sample = {'image': image, 'label': label}
        if self.transform:
            sample["image"] = self.transform(sample["image"])

        return sample

class CornDataset2(Dataset):
    def __init__(self, root_dir, transform=None):
        self.files = get_files_path(root_dir)
        self.transform = transform
    
    def __len__(self):
        return(len(self.files))
    
    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.to_list()
        
        path = self.files[idx]
        image = io.imread(path)
        # image = cv2.cvtColor(cv2.imread(path),cv2.COLOR_BGR2RGB)
        path_list = path.split("/")
        plot = path_list[-2].upper()
        if path_list[-3]=='Plot Study 1 - 09152021':
            ps = "PS1"
        elif path_list[-3]=='Plot Study 2 - 09202021':
            ps = "PS2"
        else:
            print("Error loading the image label")
        label = ps + "-" + plot

        sample = {'image': image, 'label': label}
        if self.transform:
            sample["image"] = self.transform(sample["image"])

        return sample["image"],sample["label"]